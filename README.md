Cuttlefish Theme Base
===============================

This a gem for use in Ruby on Rails applications to provide
a theme for the application.

You can use this gem directly, using configuration options to modify the
appearance, or you can fork the gem and modify the asset files.

This theme base is built using:

* Bootstrap 5
* SCSS
* HAML

Example Installation
----------------------------------------

`Gemfile`:

    gem 'theme', git: 'https://0xacab.org/liberate/nest/cuttlefish_themer.git'

`app/assets/stylesheets/application.scss`:

    @import "theme/style";

`app/controllers/application_controller.rb`:

    class ApplicationController < ActionController::Base
      layout 'application'
    end

`app/views/layout/application`

    :ruby
      theme.show_sidebar = current_user.present?
      theme.sidebar_items = {
        home: {
          label: 'Home',
          icon: 'house',
          path: main_app.home_path,
          active: active(main_app.home_path)
        },
        support: {
          label: 'Support',
          icon: 'life-preserver',
          path: support.tickets_path,
          active: active(support.tickets_path)
        }
      }
      theme.footer_items = {
        donate: {
          label: 'Donate',
          icon: 'heart',
          path: '#'
        },
        about: {
          label: 'About Us',
          icon: 'person-arms-up',
          path: '#'
        },
        policy: {
          label: 'Privacy Policy',
          icon: 'shield-check',
          path: '#'
        }
      }
      if current_user
        theme.header_status_partial = 'layouts/session'
      end
    = render template: 'layouts/theme/application'

Configuration
------------------------------------------

A theme variables are loaded from three places, in order:

1. global defaults in `cuttlefish_themer/lib/theme/default.rb`
2. app defaults in `config/initializers/theme.rb`
3. per-request variable overrides via the `theme` object.

An example `config/initializers/theme.rb`

    Theme.config.apply(
      content_bg: 'green'
    )

You can set variables for an action or controller:

    class ApplicationController < ActionController::Base
      before_filter :apply_theme
      private
      def apply_theme
        theme.apply(
          content_bg: 'red',
        )
      end
    end

Kaminari Pagination
-------------------------------------------

Cuttlefish comes with view files for pagination with Kaminari. These will only
get picked up if the cuttlefish gem is included *after* kaminari in the main
application Gemfile.

Reference
-------------------------------------------

CSS Variables: https://getbootstrap.com/docs/5.2/customize/css-variables/

Sass Variables: https://github.com/twbs/bootstrap-rubygem/blob/master/assets/stylesheets/bootstrap/_variables.scss
