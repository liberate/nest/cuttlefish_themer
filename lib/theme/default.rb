#
# Make the default variables defined in _variables.scss available to ruby code
#
module Theme::Default
  VARIABLES_PATH = File.expand_path('../../../app/assets/stylesheets/theme/_variables.scss', __FILE__)

  def self.vars
    @variables ||= load_variables(VARIABLES_PATH)
  end

  private

  def self.load_variables(file_path)
    vars = HashWithIndifferentAccess.new
    File.readlines(file_path).each do |line|
      if line =~ /^\s*\-\-/
        key, value = line.split(':')
        key = key.sub('--','').gsub('-','_').strip
        value = value.gsub(/(\!default|;|\"|url\(|\))/, '').strip
        vars[key] = value
      end
    end
    return vars
  end

  # VARS = {
  #   favicon: "/assets/theme/favicon.png",

  #   ##
  #   ## CONTENT
  #   ##
  #   content_bg: "#fff",

  #   ##
  #   ## HEADER
  #   ##
  #   header_text: "New Site",
  #   header_bg: "#ae99d1",
  #   header_fg: "#fff",
  #   header_icon_bg: "#9679c3",
  #   header_icon_url: "/assets/theme/site-icon.svg",
  #   header_icon_position: "center center",
  #   header_image_url: nil,
  #   header_image_position: "left center",

  #   ##
  #   ## SIDEBAR
  #   ##
  #   show_sidebar: false,
  #   sidebar_items: {},
  #   sidebar_icon_size: 16,
  #   sidebar_active_highlight: "var(--header-bg)",

  #   ##
  #   ## FOOTER
  #   ##
  #   footer_bg: "#eee",
  #   show_footer: true,
  #   footer_items: {}
  # }
end
