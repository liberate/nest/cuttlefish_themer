module Theme
  class Configuration < HashWithIndifferentAccess
    def method_missing(method_name, arg=nil)
      method_name = method_name.to_s
      if method_name =~ /\=/
        self[method_name.sub('=','')] = arg
      else
        fetch(method_name.sub('?',''), nil)
      end
    end

    def apply(**args)
      merge!(**args)
    end

    def apply_defaults(**args)
      reverse_merge!(**args)
    end

    def active_navigation_entry

    end

    def is_css_variable?(key)
      value = self[key]
      !key.ends_with?('_text') &&
      !key.ends_with?('_partial') &&
      key != 'favicon_url' &&
      !value.nil? && (
        value.is_a?(String) ||
        value.is_a?(Integer)
      )
    end

    def value_is_default?(key)
      self[key] == Theme::Default.vars[key]
    end

  end
end
