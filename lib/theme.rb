module Theme
  def self.configure
    yield config
  end
  def self.config
    @config ||= Configuration.new
  end
end
