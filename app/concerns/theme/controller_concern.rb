module Theme::ControllerConcern
  extend ActiveSupport::Concern
  def theme
    @theme ||= Theme.config.clone.apply_defaults(**Theme::Default.vars)
  end
end
