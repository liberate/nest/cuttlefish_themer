module Theme
  module CuttlefishHelper
    protected

    def theme
      controller.theme
    end

    def theme_sidebar_link(label:, path:, icon: nil, active: nil, class: nil)
      if active.is_a?(TrueClass)
        active = "active"
      elsif active.is_a?(FalseClass)
        active = ""
      elsif active.is_a?(Hash)
        active = self.active(active)
      end
      if icon
        text = ("%s <span>%s</span>" % [
          bootstrap_icon(icon, width: theme.sidebar_icon_size, height: theme.sidebar_icon_size),
          Rack::Utils.escape_html(label)
        ]).html_safe
      else
        text = content_tag(:span) { label }
      end
      link_to(text, path, class: "nav-link #{active}", title: label)
    end

    def theme_sidebar_section(label:, icon:)
      content_tag(:div, class: 'section') {
        icon(icon, content_tag(:span, label))
      } + content_tag(:div, '', class: 'section-rule')
    end

    def theme_favicon
      if theme.favicon_url
        content_tag(:link, '', rel: "icon", type: "image/png", href: image_path(theme.favicon_url))
      end
    end

    def theme_variables
      content_tag(:style) do
        "body {\n#{css_variable_definitions}}\n"
      end
    end

    def one_line(&block)
      haml_concat capture_haml(&block).gsub("\n", '').gsub('\\n', "\n")
    end

    #def content_if_exists(partial, flags={})
    #  if content_for?(partial)
    #    content_for(partial)
    #  elsif lookup_context.template_exists?(partial, 'layouts', true)
    #    render "layouts/#{partial}"
    #  else
    #    ""
    #  end
    #end

    #
    # loads an application's 'layouts/theme' partial, allow it to
    # define variables and content blocks.
    #
    #def load_custom_theme
    #  if lookup_context.template_exists?('theme', 'layouts', true)
    #    render 'layouts/theme'
    #  end
    #  nil
    #end


    #
    # Generates HTML markup from flash messages.
    #
    # messages argument may be a String, Array, Exception, or ActiveModel::Errors
    #
    # Example:
    #
    # - flash.each do |name, messages|
    #   = content_tag :div, class: "alert alert-#{name}" do
    #     = flash_messages(messages)
    #
    def flash_messages(messages)
      if messages.nil? || (messages.respond_to?(:empty?) && messages.empty?)
        return nil
      end

      bullets = []
      messages = [messages].flatten

      messages.each do |msg|
        if msg.is_a?(String)
          bullets << safe_format(msg)
        elsif msg.respond_to?(:errors)
          msg.errors.full_messages.each do |i|
            bullets << safe_format(i)
          end
        elsif msg.respond_to?(:full_messages)
          msg.full_messages.each do |i|
            bullets << safe_format(i)
          end
        elsif msg.is_a?(ErrorMessage)
          msg.messages.each do |i|
            bullets << safe_format(i)
          end
        elsif msg.is_a?(StandardError)
          bullets << safe_format(msg.to_s)
        elsif msg.is_a?(Array) && msg.any?
          msg.each do |i|
            bullets << safe_format(i)
          end
        end
      end

      if bullets.size == 1
        return bullets.first
      else
        return content_tag(:ul) {
          bullets.map{|b| content_tag(:li, b)}.join("\n").html_safe
        }
      end
    end

    private

    def css_variable_definitions
      theme.keys.filter_map {|key|
        if theme.is_css_variable?(key) && !theme.value_is_default?(key)
          value = if key.ends_with?('_url')
            "url(%s)" % image_path(theme[key])
          else
            theme[key]
          end
          css_key = key.gsub('_', '-')
          "--#{css_key}: #{value};"
        end
      }.join("\n")
    end

    ##
    ## FORMATTING FLASH
    ##

    SAFE_FORMAT_MAP = {
      "[b]"  => "<b>",
      "[/b]" => "</b>",
      "[r]"  => "<span style='color:red; font-weight: bold'>",
      "[/r]" => "</span>"
    }

    SAFE_FORMAT_RE = Regexp.union(SAFE_FORMAT_MAP.keys)

    def safe_format(str)
      h(str).gsub(SAFE_FORMAT_RE, SAFE_FORMAT_MAP).html_safe
    end

  end
end